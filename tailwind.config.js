/** @type {import('tailwindcss').Config} */
module.exports = {
  presets: [require("@bmonline/theme/tailwindConfig")],
  content: [
    "./src/**/*.{html,ts}",
    "./node_modules/@bmonline/**/*.{html,ts,js,mjs}",
  ],
};
